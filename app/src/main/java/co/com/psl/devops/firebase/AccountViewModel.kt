package co.com.psl.devops.firebase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class AccountViewModel : ViewModel() {
    private var user: MutableLiveData<FirebaseUser>? = null
    fun getUsers(): LiveData<FirebaseUser> {
        if (user == null) {
            user = MutableLiveData()
            loadUser()
        }
        return user!!
    }

    private fun loadUser() {
        user!!.postValue( FirebaseAuth.getInstance().currentUser )
    }
}